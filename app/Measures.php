<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Measures extends Model
{
    use Notifiable;
    /**
     * Measures belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    	// belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
    	return $this->belongsTo(User::class);
    }


    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['user_id'];
}
