<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('raskrsnica');
            $table->enum('pozicija',['1','2','3','4']);
            $table->enum('strana',['1','2','3','4']);
            $table->string('vreme');
            $table->enum('smer',['1.2','1.3','1.4','2.1','2.3','2.4','3.1','3.2','3.4','4.1','4.2','4.3']);
            $table->enum('dan',['pon','uto','sre','cet','pet','sub','ned']);
            
            $table->timestamps('datum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measures');
    }
}
