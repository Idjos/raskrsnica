<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i <= 50; $i++) {
            DB::table( 'users' )->insert( [
                'name' => $faker->firstName(),
                'lastname' => $faker->lastname(),
                'email' => $faker->unique()->email(),
                'password' => bcrypt('123'),
            ] );
        }
        DB::table( 'users' )->insert( [
            'name' => 'Filip',
            'lastname' => 'Tonic',
            'email' => 'tonke@vts.rs',
            'password' => bcrypt('1234'),
        ] );
    }
}
