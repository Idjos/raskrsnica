<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'UserController@index');

    Route::get('/home', 'UserController@index');

    Route::get('delete/user/{id}', 'UserController@destroy');

    Route::post('edit/user/{id}', 'UserController@update');

    Route::post('add/user', 'UserController@store');

    Route::get('logout','Auth\LoginController@logout');

    Route::get('deleteAll', 'UserController@deleteAll');

    Route::get('all','MeasuresController@allMeasures');

    Route::post('add/measure','MeasuresController@store');

    
    
});

