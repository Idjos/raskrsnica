 <div id="edit{{$user->id}}" class="modal fade" role="dialog">

                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <h4 class="modal-title">Izmeni</h4>

                        </div>

                        <form method="POST" action="edit/user/{{$user->id}}">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <div class="modal-body">

                                <label>Ime:</label>

                                <input name="name" type="text" class="form-control" value="{{$user->name}}" placeholder="Name" required>

                                <br>

                                <label>Prezime:</label>

                                <input name="lastname" type="text" class="form-control" value="{{$user->lastname}}" placeholder="Lastname" required>

                                <br>

                                <label>Email:</label>

                                <input name="email" type="email" class="form-control" value="{{$user->email}}" placeholder="example@demo.com" required>

                                <br>

                                <label>Nova Lozinka:</label>

                                <input name="password" type="password" class="form-control" placeholder="*********">

                                <br>

                                <label>Novi Broj Indeksa:</label>

                                <input  name="Indeks" class="form-control" placeholder="br / br" required>

                            </div>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>

                                <button type="submit" class="btn btn-primary btn-classic-style">Sacuvaj</button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>