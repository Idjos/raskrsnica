<div id="addNew" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dodaj Studenta</h4>
            </div>

            <form method="POST" action="add/user">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="modal-body">

                    <label>Ime:</label>

                    <input name="name" type="text" class="form-control" placeholder="Ime" required>

                    <br>

                    <label>Prezime:</label>

                    <input name="lastname" type="text" class="form-control" placeholder="Prezime" required>

                    <br>

                    <label>Broj Indeksa</label>

                    <input name="brInd" type="text" class="form-control" placeholder="br / br" required>

                    <br>

                    <label>Email:</label>

                    <input name="email" type="email" class="form-control" placeholder="example@demo.com" required>

                    <br>

                    <label>Lozinka:</label>

                    <input name="password" type="password" class="form-control" placeholder="*********">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>
                    <button type="submit" class="btn btn-primary btn-classic-style">Sacuvaj</button>
                </div>
            </form>
        </div>

    </div>
</div>