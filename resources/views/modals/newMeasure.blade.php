<div id="addNewMeasure" class="modal fade" role="dialog">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title">Novo Merenje</h4>

            </div>

            <form method="POST" action="add/measure">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="modal-body">

                    @include('Measures.addMeasure')

                </div>





                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>

                    <button type="submit" class="btn btn-primary btn-classic-style">Sacuvaj</button>

                </div>

            </form>
        </div>

    </div>
</div>