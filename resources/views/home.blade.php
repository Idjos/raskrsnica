@extends('welcome')

@section('content')

<div class="row">

    <div class="col-xs-12">

        <div class="row">

            <div class="col-xs-12 col-sm-10">

                <h2>Registrovani Studenti</h2>

            </div>



            <div class="add-new-btn col-xs-16 col-sm-2">

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNew">Dodaj Studenta</button>

            </div>


            <div class="add-new-btn col-xs-16 col-sm-2">

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNewMeasure">Novo Merenje</button>

            </div>

 

<form method="GET" action="deleteAll">



    <div class="add-new-btn col-xs-16 col-sm-2">

      <th>

        <button type="submit" class="btn-danger btn-primary">Obrisi Studente</button>

      </th>

    </div>

</form>


        <table class="table user-table">

            <thead>

                <tr>

                    <th>Ime</th>

                    <th>Prezime</th>

                    <th>Indeks</th>

                    <th>Email</th>

                    <th> Merenja</th>

                    <th>Izmene</th>

                    <th>Brisanje</th>

                </tr>

            </thead>

            <tbody>

            @foreach($users as $user)

                <tr>

                    <td>{{$user->name}}</td>

                    <td>{{$user->lastname}}</td>

                    <td> {{$user->brInd}} </td>

                    <td>{{$user->email}}</td>

                    <th>

                        <button type="button" class="btn btn-success btn-classic-style" data-toggle="modal" data-target="#viewDetails{{$user->id}}"> Merenja </button>

                    </th>

                    <th>

                        <button type="button" class="btn btn-info btn-classic-style" data-toggle="modal" data-target="#edit{{$user->id}}">Izmeni</button>

                    </th>

                    <form method="GET" action="delete/user/{{$user->id}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <th>

                                <button type="submit" class="btn btn-danger btn-classic-style">Obrisi</button>

                            </th>

                    </form>

                </tr>

            </tbody>

            <!-- Modal edit -->
           @include('modals.izmeniStudenta')

            
           @include('Measures.Merenja')

            @endforeach

        </table>

        <div style="float: right">

            {{$users->links()}}

        </div>

    </div>

</div>

<!-- Modal add new -->

@include('modals.dodajStudenta')




@include('modals.NewMeasure')


@endsection
