@extends('welcome')

@section('content')

<div class="row">

    <div class="col-xs-12">

        <div class="row">

            <div class="col-xs-12 col-sm-10">

                <h2>Registrovani Studenti</h2>

            </div>

            <div class="add-new-btn col-xs-16 col-sm-2">

                <button type="button" class="btn btn-classic-style" data-toggle="modal" data-target="#addNew">Dodaj Studenta</button>

            </div>


<form method="GET" action="deleteAll">

    <div class="add-new-btn col-xs-12 col-sm-2">

      <th>

        <button type="submit" class="btn-danger btn-classic-style">Obrisi Studente</button>

      </th>

    </div>

</form>


        <table class="table user-table">

            <thead>

                <tr>

                    <th>Ime</th>

                    <th>Prezime</th>

                    <th>Indeks</th>

                    <th>Email</th>

                    <th> Merenja</th>

                    <th>Izmene</th>

                    <th>Brisanje</th>

                </tr>

            </thead>

            <tbody>

            @foreach($users as $user)

                <tr>

                    <td>{{$user->name}}</td>

                    <td>{{$user->lastname}}</td>

                    <td> {{$user->brInd}} </td>

                    <td>{{$user->email}}</td>

                    <th>

                        <button type="button" class="btn btn-success btn-classic-style" data-toggle="modal" data-target="#viewDetails{{$user->id}}"> Merenja </button>

                    </th>

                    <th>

                        <button type="button" class="btn btn-info btn-classic-style" data-toggle="modal" data-target="#edit{{$user->id}}">Izmeni</button>

                    </th>

                    <form method="GET" action="delete/user/{{$user->id}}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <th>

                                <button type="submit" class="btn btn-danger btn-classic-style">Obrisi</button>

                            </th>

                    </form>

                </tr>

            </tbody>

            <!-- Modal edit -->
            <div id="edit{{$user->id}}" class="modal fade" role="dialog">

                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <h4 class="modal-title">Izmeni</h4>

                        </div>

                        <form method="POST" action="edit/user/{{$user->id}}">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <div class="modal-body">

                                <label>Ime:</label>

                                <input name="name" type="text" class="form-control" value="{{$user->name}}" placeholder="Name" required>

                                <br>

                                <label>Prezime:</label>

                                <input name="lastname" type="text" class="form-control" value="{{$user->lastname}}" placeholder="Lastname" required>

                                <br>

                                <label>Email:</label>

                                <input name="email" type="email" class="form-control" value="{{$user->email}}" placeholder="example@demo.com" required>

                                <br>

                                <label>Nova Lozinka:</label>

                                <input name="password" type="password" class="form-control" placeholder="*********">

                                <br>

                                <label>Novi Broj Indeksa:</label>

                                <input  name="Indeks" class="form-control" placeholder="br / br" required>

                            </div>

                            <div class="modal-footer">

                                <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>

                                <button type="submit" class="btn btn-primary btn-classic-style">Sacuvaj</button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

            <!-- Modal edit -->
            <div id="viewDetails{{$user->id}}" class="modal fade" role="dialog">

                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <h4 class="modal-title">Merenja</h4>

                        </div>

                        <div class="modal-body">

                            <label>Raskrsnica:</label>

                            <p>{{$user->name}}</p>

                            <br>

                            <label>Mesto:</label>

                            <p>{{$user->lastname}}</p>

                            <br>

                            <label>Strana:</label>

                            <p>{{$user->email}}</p>

                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>

                        </div>

                    </div>


                </div>

            </div>

            @endforeach

        </table>

        <div style="float: right">

            {{$users->links()}}

        </div>

    </div>

</div>

<!-- Modal add new -->
<div id="addNew" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dodaj Studenta</h4>
            </div>

            <form method="POST" action="add/user">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <div class="modal-body">

                    <label>Ime:</label>

                    <input name="name" type="text" class="form-control" placeholder="Ime" required>

                    <br>

                    <label>Prezime:</label>

                    <input name="lastname" type="text" class="form-control" placeholder="Prezime" required>

                    <br>

                    <label>Broj Indeksa</label>

                    <input name="brInd" type="text" class="form-control" placeholder="br / br" required>

                    <br>

                    <label>Email:</label>

                    <input name="email" type="email" class="form-control" placeholder="example@demo.com" required>

                    <br>

                    <label>Lozinka:</label>

                    <input name="password" type="password" class="form-control" placeholder="*********">

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-classic-style" data-dismiss="modal">Zatvori</button>
                    <button type="submit" class="btn btn-primary btn-classic-style">Sacuvaj</button>
                </div>
            </form>
        </div>

    </div>
</div>

@endsection
